package lib

const (
	DiveHarderStatsRoute         = "/raw/planetstats"
	DiveHarderPlanetsRoute       = "/planets"
	DiveHarderPlanetsActiveRoute = "/planets/active"
	HelldiversNewsFeedRoute      = "/NewsFeed/%s?fromTimeStamp=%d"
	HelldiversAssignmentRoute    = "/v2/Assignment/War/%s"
	HelldiversStatusRoute        = "/WarSeason/%s/status"
	GoDiversFeedRoute            = "/feed"
	GoDiversOrderRoute           = "/order"
	GoDiversPlanetsNameRoute     = "/planets"
	GoDiversPlanetRoute          = "/planets/%d"
	GoDiversGalaxyStatsRoute     = "/galaxy"
	GoDiversCampaignsRoute       = "/campaigns"
)
